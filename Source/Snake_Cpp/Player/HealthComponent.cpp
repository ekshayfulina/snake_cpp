// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/HealthComponent.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
	
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UHealthComponent::GetCurrentHealth()
{
	return Health;
}

void UHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

void UHealthComponent::ChangeHealthValue(float ChangeHealthValue)
{
	/*ChangeHealthValue = ChangeHealthValue * CoefHealthChanging;*/
	Health += ChangeHealthValue;

	//need to HealthCHangeValue broadcast
	OnHealthChange.Broadcast(Health, ChangeHealthValue);

	if (Health > 10.0f)
	{
		Health = 10.0f;
	}
	else
		if (Health < 0.0f)
		{
			//broadcast Dead
			OnDead.Broadcast();
		}
}

