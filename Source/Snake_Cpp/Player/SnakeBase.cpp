// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeBaseController.h" 
#include "Snake_CppPlayerState.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "PlayerPawnBase.h"
#include "ParentFood.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 20.f;
	//CurrentLife = 1;
	//Points = 0;
	LastMoveDirection = EMovementDirection::DOWN;

	SnakeHealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("SnakeHealthComponent"));

	if (SnakeHealthComponent)
	{
		SnakeHealthComponent->OnDead.AddDynamic(this, &ASnakeBase::DestroySnake);
	}
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	// Get the Player Controller using the custom class
	ASnakeBaseController* PlayerController = Cast<ASnakeBaseController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	if (PlayerController)
	{
		SnakePlayerState = Cast<ASnake_CppPlayerState>(PlayerController->PlayerState);
	}

	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->SnakeOwner = this;
		NewSnakeElement->SetActorHiddenInGame(true);

		int32 ElementIndex = SnakeElements.Add(NewSnakeElement);
		if (ElementIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();

		}
	}
}

void ASnakeBase::Move()
{
	FVector	MovementVector(0);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		SnakeElements[0]->SetActorHiddenInGame(false);
		SnakeElements[i]->SetActorHiddenInGame(false);
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	SetIsCanMove(false);
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	/*Moving = false;*/
}

void ASnakeBase::SetIsCanMove(bool IsValue)
{
	bIsMovingActive = IsValue;
}

bool ASnakeBase::GetIsCanMove()
{
	return bIsMovingActive;
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase * OverlappedElement, AActor * Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElementIndex;
		SnakeElements.Find(OverlappedElement, ElementIndex);
		bool bIsFirst = ElementIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);

		}
	}
}

void ASnakeBase::ChangeSpeed(float Amount)
{
	MovementSpeed += Amount;
	SetActorTickInterval(MovementSpeed);
	if (MovementSpeed <= 1)
		return;
}

//void ASnakeBase::Changelife(int32 Amount)
//{
//	CurrentLife += Amount;
//	if (CurrentLife <= 0)
//	{
//		DestroySnake();
//	}
//}

//void ASnakeBase::ChangeScore(int32 ChangeScoreValue)
//{
//	Points += ChangeScoreValue;
//	OnScoreChange.Broadcast(Points,ChangeScoreValue);
//
//	if (Points >= 10)
//	{
//		DestroySnake();
//	}
//}


void ASnakeBase::DestroySnake()
{
	for (int i = SnakeElements.Num() - 1; i >= 0; i--)
	{
		SnakeElements[i]->Destroy();
		//play Death Sound
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), DeathSound, GetActorLocation());
	}
	this->Destroy();
}