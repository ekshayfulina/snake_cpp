// Copyright Epic Games, Inc. All Rights Reserved.

#include "Snake_Cpp.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Snake_Cpp, "Snake_Cpp" );
