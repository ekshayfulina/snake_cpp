// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedFood.h"
#include "SnakeBase.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"

void ASpeedFood::Interact(AActor* actor, bool isHead)
{
	if (isHead)
	{
		ASnakeBase* Snake = Cast<ASnakeBase>(actor);
		if (IsValid(Snake))
		{
			//play Slow Food Sound
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), SpeedFoodSound, GetActorLocation());

			Snake->ChangeSpeed(-AddSpeed);
			// Show the widget
			ShowSpeedUpWidget();

			Destroy();
		}
	}
}

void ASpeedFood::ShowSpeedUpWidget()
{
	if (SpeedUpWidgetClass)
	{
		if (!SpeedUpWidget)
		{
			SpeedUpWidget = CreateWidget<UUserWidget>(GetWorld(), SpeedUpWidgetClass);
		}
		
		if (SpeedUpWidget)
		{
			SpeedUpWidget->AddToViewport();

			// Set a timer
			/*GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShowWidget, this, &ASpeedFood::HideSpeedUpWidget, 3.0f, false);*/
			// Call static func
			ManageSpeedUpWidget(SpeedUpWidget, 5.0f, GetWorld());
		}
	}
}

void ASpeedFood::HideSpeedUpWidget()
{
	if (SpeedUpWidget)
	{
		SpeedUpWidget->RemoveFromParent();
		SpeedUpWidget = nullptr; // Clear the pointer
	}
}

void ASpeedFood::ManageSpeedUpWidget(UUserWidget* Widget, float Duration, UWorld* World)
{
	if (Widget && World)
	{
		FTimerHandle TimerHandle;
		World->GetTimerManager().SetTimer(TimerHandle, [Widget]()
			{
				if (Widget)
				{
					Widget->RemoveFromParent();
				}
			}, Duration, false);
	}
}
