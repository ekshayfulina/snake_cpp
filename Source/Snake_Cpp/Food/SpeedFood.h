// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ParentFood.h"
#include "Blueprint/UserWidget.h"
#include "Sound/SoundBase.h"
#include "SpeedFood.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_CPP_API ASpeedFood : public AParentFood
{
	GENERATED_BODY()

public:
	void Interact(AActor* actor, bool isHead) override;

protected:
	//Speed Food Sound
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	USoundBase* SpeedFoodSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TSubclassOf<UUserWidget> SpeedUpWidgetClass;

	UUserWidget* SpeedUpWidget = nullptr;
private:
	void ShowSpeedUpWidget();
	void HideSpeedUpWidget();
	// Static function to manage widget display
	static void ManageSpeedUpWidget(UUserWidget* Widget, float Duration, UWorld* World);
};
