// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ParentFood.h"
#include "Blueprint/UserWidget.h"
#include "Sound/SoundBase.h"
#include "SlowFood.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_CPP_API ASlowFood : public AParentFood
{
	GENERATED_BODY()

public:
	void Interact(AActor* actor, bool isHead) override;
	
protected:
	//Slow Food Sound
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	USoundBase* SlowFoodSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TSubclassOf<UUserWidget> SpeedDownWidgetClass;

	UUserWidget* SpeedDownWidget = nullptr;
private:
	void ShowSpeedDownWidget();
	void HideSpeedDownWidget();
	// Static function to manage widget display
	static void ManageSpeedDownWidget(UUserWidget* Widget, float Duration, UWorld* World);
};
