// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseFood.h"
#include "Kismet/GameplayStatics.h"

void ABaseFood::Interact(AActor* actor, bool isHead)
{
	if (isHead)
	{
		ASnakeBase* Snake = Cast<ASnakeBase>(actor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();

			//play Add Score Sound
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), AddScoreSound, GetActorLocation());
			
			Snake->SnakePlayerState->ChangeScore(AddScore);
			Destroy();
		}
	}
}
