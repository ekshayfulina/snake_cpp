// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "ParentFood.generated.h"

UCLASS()
class SNAKE_CPP_API AParentFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AParentFood();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* actor, bool isHead) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float AddLife = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 AddScore = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float AddSpeed = 0.4;

};
