// Fill out your copyright notice in the Description page of Project Settings.


#include "ParentFood.h"
#include "SnakeBase.h"

// Sets default values
AParentFood::AParentFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AParentFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AParentFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AParentFood::Interact(AActor* actor, bool isHead)
{
	Destroy();
}

