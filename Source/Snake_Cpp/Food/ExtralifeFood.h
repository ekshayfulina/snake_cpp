// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ParentFood.h"
#include "Sound/SoundBase.h"
#include "ExtralifeFood.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_CPP_API AExtralifeFood : public AParentFood
{
	GENERATED_BODY()

public:
	void Interact(AActor* actor, bool isHead) override;

protected:
	//Add Life Sound
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	USoundBase* AddLifeSound;
	
};
