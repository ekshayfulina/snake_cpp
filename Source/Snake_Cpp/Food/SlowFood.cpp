// Fill out your copyright notice in the Description page of Project Settings.


#include "SlowFood.h"
#include "SnakeBase.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"

void ASlowFood::Interact(AActor* actor, bool isHead)
{
	if (isHead)
	{
		ASnakeBase* Snake = Cast<ASnakeBase>(actor);
		if (IsValid(Snake))
		{
			//play Slow Food Sound
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), SlowFoodSound, GetActorLocation());

			Snake->ChangeSpeed(AddSpeed);
			ShowSpeedDownWidget();

			Destroy();
		}
	}
}

void ASlowFood::ShowSpeedDownWidget()
{
	if (SpeedDownWidgetClass)
	{
		if (!SpeedDownWidget)
		{
			SpeedDownWidget = CreateWidget<UUserWidget>(GetWorld(), SpeedDownWidgetClass);
		}

		if (SpeedDownWidget)
		{
			SpeedDownWidget->AddToViewport();

			// Set a timer
			/*GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShowWidget, this, &ASpeedFood::HideSpeedUpWidget, 3.0f, false);*/
			// Call static func
			ManageSpeedDownWidget(SpeedDownWidget, 5.0f, GetWorld());
		}
	}
}

void ASlowFood::HideSpeedDownWidget()
{
	if (SpeedDownWidget)
	{
		SpeedDownWidget->RemoveFromParent();
		SpeedDownWidget = nullptr; // Clear the pointer
	}
}

void ASlowFood::ManageSpeedDownWidget(UUserWidget* Widget, float Duration, UWorld* World)
{
	if (Widget && World)
	{
		FTimerHandle TimerHandle;
		World->GetTimerManager().SetTimer(TimerHandle, [Widget]()
			{
				if (Widget)
				{
					Widget->RemoveFromParent();
				}
			}, Duration, false);
	}
}
