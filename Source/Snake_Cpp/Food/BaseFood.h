// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ParentFood.h"
#include "SnakeBase.h"
#include "Sound/SoundBase.h"
#include "BaseFood.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_CPP_API ABaseFood : public AParentFood
{
	GENERATED_BODY()

public:
	void Interact(AActor* actor, bool isHead) override;

protected:
	//Add Score Sound
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	USoundBase* AddScoreSound;
};
