// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ParentFood.h"
#include "Sound/SoundBase.h"
#include "ReduceLifeFood.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_CPP_API AReduceLifeFood : public AParentFood
{
	GENERATED_BODY()
public:
	void Interact(AActor* actor, bool isHead) override;

protected:
	//Reduce Life Sound
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	USoundBase* ReduceLifeSound;	
};
