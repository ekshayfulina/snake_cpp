// Fill out your copyright notice in the Description page of Project Settings.


#include "ExtralifeFood.h"
#include "SnakeBase.h"
#include "Kismet/GameplayStatics.h"

void AExtralifeFood::Interact(AActor* actor, bool isHead)
{
	if (isHead)
	{
		ASnakeBase* Snake = Cast<ASnakeBase>(actor);
		if (IsValid(Snake))
		{
			//Snake->Changelife(AddLife);
			Snake->SnakeHealthComponent->ChangeHealthValue(AddLife);

			//play Add Life Sound
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), AddLifeSound, GetActorLocation());

			Destroy();
		}	
	}
}
