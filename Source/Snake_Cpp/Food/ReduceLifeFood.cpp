// Fill out your copyright notice in the Description page of Project Settings.


#include "ReduceLifeFood.h"
#include "SnakeBase.h"
#include "Kismet/GameplayStatics.h"

void AReduceLifeFood::Interact(AActor* actor, bool isHead)
{
	if (isHead)
	{
		ASnakeBase* Snake = Cast<ASnakeBase>(actor);
		if (IsValid(Snake))
		{
			/*Snake->Changelife(-AddLife);*/
			Snake->SnakeHealthComponent->ChangeHealthValue(-AddLife);

			//play Reduce Life Sound
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), ReduceLifeSound, GetActorLocation());

			Destroy();
		}
	}
}
