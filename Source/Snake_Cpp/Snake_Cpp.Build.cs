// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Snake_Cpp : ModuleRules
{
	public Snake_Cpp(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });

		PrivateDependencyModuleNames.AddRange(new string[] { });

        PublicIncludePaths.AddRange(new string[]
            {
                 "Snake_Cpp",
                 "Snake_Cpp/Food",
                 "Snake_Cpp/Game",
                 "Snake_Cpp/Player",
                 "Snake_Cpp/Widgets"
            });

        // Uncomment if you are using Slate UI
        // PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

        // Uncomment if you are using online features
        // PrivateDependencyModuleNames.Add("OnlineSubsystem");

        // To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
    }
}
                                                                                                           