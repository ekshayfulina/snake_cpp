// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Snake_CppGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_CPP_API ASnake_CppGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
