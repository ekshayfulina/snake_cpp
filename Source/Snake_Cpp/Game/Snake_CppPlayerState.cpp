// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/Snake_CppPlayerState.h"


int32 ASnake_CppPlayerState::GetCurrentScore()
{
	return CurrentScore;
}

void ASnake_CppPlayerState::SetCurrentScore(int32 NewCurrentScore)
{
	CurrentScore = NewCurrentScore;
}

void ASnake_CppPlayerState::ChangeScore(int32 ChangeScoreValue)
{

	CurrentScore += ChangeScoreValue;
	OnScoreChange.Broadcast(CurrentScore, ChangeScoreValue);

}
