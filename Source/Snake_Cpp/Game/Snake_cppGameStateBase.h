// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "SnakeBase.h" 
#include "Snake_CppPlayerState.h"
#include "Snake_cppGameStateBase.generated.h"

/**
 * 
 */

UCLASS()
class SNAKE_CPP_API ASnake_cppGameStateBase : public AGameStateBase
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Score")
	int32 WinScore;

	 //UPROPERTY()
	 //UHealthComponent* SnakeHealth;
	UPROPERTY()
	ASnakeBase* SnakeActor;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// New method to be called directly
	void OnSnakeActorCreated(ASnakeBase* NewSnakeActor);

	UFUNCTION(BlueprintCallable)
	void CheckWinCondition(int32 CurrentScore, int32 ChangeScore);

	UFUNCTION(BlueprintNativeEvent)
	void WinGame();
};
