// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/Snake_cppGameStateBase.h" 
#include "SnakeBaseController.h"

void ASnake_cppGameStateBase::BeginPlay()
{
	Super::BeginPlay();

	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	if (PlayerController)
	{
		// Cast to your custom controller
		ASnakeBaseController* SnakeBaseController = Cast<ASnakeBaseController>(PlayerController);

		if(SnakeBaseController)
		{
			ASnake_CppPlayerState* PlayerState = Cast<ASnake_CppPlayerState>(SnakeBaseController->PlayerState);

			if (PlayerState)
			{
				PlayerState->OnScoreChange.AddDynamic(this, &ASnake_cppGameStateBase::CheckWinCondition);
			}
			SnakeActor = Cast<ASnakeBase>(SnakeBaseController->GetPawn());
		}
	}
}

void ASnake_cppGameStateBase::OnSnakeActorCreated(ASnakeBase* NewSnakeActor)
{
	SnakeActor = NewSnakeActor;
	// Set up any additional logic here, like binding events if needed
}


void ASnake_cppGameStateBase::CheckWinCondition(int32 CurrentScore, int32 ChangeScore)
{
	if (CurrentScore >= WinScore && SnakeActor)
	{
		/*SnakeHealth->OnDead.Broadcast();*/
		SnakeActor->DestroySnake();
		WinGame();
	}

}

void ASnake_cppGameStateBase::WinGame_Implementation()
{
	//in BP
}
