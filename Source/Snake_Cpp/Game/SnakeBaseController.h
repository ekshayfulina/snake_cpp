// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SnakeBaseController.generated.h"

/**
 * 
 */

UCLASS()
class SNAKE_CPP_API ASnakeBaseController : public APlayerController
{
	GENERATED_BODY()
	
public:

	ASnakeBaseController();

};
