// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
//#include "Snake_cppGameStateBase.h"
//#include "HealthComponent.h"
#include "Snake_CppPlayerState.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnScoreChange, int32, CurrentScore, int32, ChangeScore);

UCLASS()
class SNAKE_CPP_API ASnake_CppPlayerState : public APlayerState
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Score")
	FOnScoreChange OnScoreChange;

	//UPROPERTY()
	//ASnake_cppGameStateBase* SnakeGameBaseState;   

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Score")
	int32 CurrentScore = 0;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Score")
	//int32 WinScore;

public:
	UFUNCTION(BlueprintCallable, Category = "Score")
	int32 GetCurrentScore();
	UFUNCTION(BlueprintCallable, Category = "Score")
	void SetCurrentScore(int32 NewCurrentScore);

	UFUNCTION(BlueprintCallable, Category = "Score")
    void ChangeScore(int32 ChangeScoreValue);

};
